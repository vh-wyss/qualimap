/**
 * QualiMap: evaluation of next generation sequencing alignment data
 * Copyright (C) 2016 Garcia-Alcalde et al.
 * http://qualimap.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
package org.bioinfo.ngs.qc.qualimap.common;

/**
 * Created by kokonech
 * Date: 2/9/12
 * Time: 11:14 AM
 */

public class ReadStartsHistogram {

    int maxReadStartsPerPosition = Constants.DEFAULT_DUPL_RATE_HIST_MAX;
    long currentReadStartPosition;
    int readStartCounter;
    long[] readStartsHistogram;

    public ReadStartsHistogram() {
        readStartsHistogram = new long[maxReadStartsPerPosition + 1];
        readStartCounter = 1;
        currentReadStartPosition = -1;
    }

    public void setMaxReadStartsPerPosition(int maxReadStartsPerPosition) {
        this.maxReadStartsPerPosition = maxReadStartsPerPosition;
        this.readStartsHistogram = new long[maxReadStartsPerPosition + 1];
    }

    public int getMaxReadStartsPerPosition() {
        return maxReadStartsPerPosition;
    }
    public boolean update( long position ) {
        if (position == currentReadStartPosition) {
            readStartCounter++;
        } else {
            int histPos = readStartCounter < maxReadStartsPerPosition ?  readStartCounter :
                    maxReadStartsPerPosition;
            readStartsHistogram[histPos]++;
            readStartCounter = 1;
            currentReadStartPosition = position;
        }

        return readStartCounter > 1;
    }

    public long[] getHistorgram() {
        return readStartsHistogram;
    }


}
